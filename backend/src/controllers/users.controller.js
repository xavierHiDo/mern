const userCtrl = {};

const User = require("../models/User");

userCtrl.getUsers = async (req, res) => {
  try {
    let users = await User.find();
    res.json(users);
  } catch (err) {
    res.status(400).json({
      error: err,
    });
  }
};

userCtrl.createUser = async (req, res) => {
  try {
    let { username } = req.body;

    let newUser = new User({ username });
    await newUser.save();
    res.json("User created");
  } catch (e) {
    console.log(e);
    res.json(e.errmsg);
  }
};

userCtrl.deleteUser = async (req, res) => {
  let { id } = req.params;
  await User.findByIdAndDelete(id);
  res.json("User deleted");
};

module.exports = userCtrl;
